package DAO;
import beans.GiteSearched;
import metier.*;
import java.sql.*;

import java.util.ArrayList;


public class GiteDAO extends DAO<Gite>{
    private int chambreMax;
    private int chambremin;
    public GiteDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public Gite getByID(int id) {
        Gite gite = new Gite(0, "");
        ResultSet rs;
        Statement stm = null;
        try {
            stm = connexion.createStatement();
            String strCmd = "SELECT g.id_gite,nom_gite,\n" +
                    "                        surface_habitable,nombre_chambres, nombre_couchage,site_web,adresse_1,adresse_2,\n" +
                    "                                                   adresse_3,libelle_voie,g.code_postale,\n" +
                    "                        c2.code_insee,c2.nom_commune,c2.code_postale,c2.latitude,c2.longitude,\n" +
                    "                        p1.id_personne,p1.nom_personne,p1.prenom_personne,p1.email,\n" +
                    "                        p2.id_personne,p2.nom_personne,p2.prenom_personne,p2.email,\n" +
                    "                        d.code_departement,d.nom_departement,\n" +
                    "                        r.code_region,r.nom_region\n" +
                    "                        from gite\n" +
                    "                        as G join commune c2 on G.code_departement = c2.code_departement and G.code_insee = c2.code_insee\n" +
                    "                            JOIN est_gere_par E on E.id_gite=g.id_gite\n" +
                    "                           join personne as p1 ON E.id_personne=p1.id_personne\n" +
                    "                            JOIN avoir A on A.id_gite=g.id_gite\n" +
                    "                           join personne as p2 ON E.id_personne=p2.id_personne\n" +

                    "                        join  departement d on c2.code_departement = d.code_departement\n" +
                    "                        JOIN region r on d.code_region = r.code_region\n" +
                    "                         where g.id_gite = "  +  id   +"order by id_gite";

            rs = stm.executeQuery(strCmd);



            Ville ville;
            Departement departement;
            while(rs.next()){
                gite.setIdGite(rs.getInt(1));
                gite.setNomGite( rs.getString(2));
                gite.setNombreChambres(rs.getInt(4));
                gite.setNombreCouchages(rs.getInt(5));
                gite.setAdresse1(rs.getString(7));
                gite.setAdresse2(rs.getString(8));
                gite.setAdresse3(rs.getString(9));
                gite.setLibelleVoie(rs.getString(10));
                gite.setCodePostale(rs.getString(11));
                gite.setSurfaceHabitable(rs.getFloat(3));
                gite.setSiteWeb(rs.getString(6));
                ville= new Ville(rs.getString(12), rs.getString(13),
                        rs.getString(14),rs.getFloat(15),
                        rs.getFloat(16));
                gite.setVille(ville);
                departement= new Departement(rs.getString(25),rs.getString(26));
                ville.setDepartement(departement);
                departement.setRegion(new Region(rs.getString(27),rs.getString(28)));
                gite.setGerants(listerGerants(rs.getInt(1)));
                gite.setProprietaires(listerProprietaire(rs.getInt(1)));
                gite.setPrestations(listerPrestations(rs.getInt(1)));
                gite.setTarif(recupererTarifGite(rs.getInt(1)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return gite;
    }
    public void getBayId (int id){


    }

    public ArrayList<Gite> getLike(GiteSearched giteSearched) {
        ResultSet rs;
        ArrayList <Gite>  liste = new ArrayList<Gite>();
        String strCmd = " { call   public.gite_qbe (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        try {
            CallableStatement stm = this.connexion.prepareCall(strCmd);
            if (giteSearched.getNomGite().equals(""))
                stm.setNull(1,Types.VARCHAR );
            else {stm.setString(1,giteSearched.getNomGite());}
            if (giteSearched.getNombreChambresMax()> 117)
                stm.setNull(2, Types.INTEGER);
            else
                stm.setInt(2, giteSearched.getNombreChambresMax());
            if (giteSearched.getNombreChambresMax()< 1)
                stm.setNull(3, Types.INTEGER);
            else
                stm.setInt(3, giteSearched.getNombreChambresMin());

            stm.setInt(4,giteSearched.getNombreCouchagesMax());
            stm.setInt(5,giteSearched.getNombreCouchagesMin());
            if (giteSearched.getVille() == null){
                stm.setNull(6, Types.VARCHAR);
                stm.setNull(14,Types.FLOAT);
                stm.setNull(15, Types.FLOAT);}
            else
            { stm.setString(6, giteSearched.getVille().getCodeInsee());
                stm.setFloat(14,giteSearched.getVille().getLatitude());
                stm.setFloat(15,giteSearched.getVille().getLongitude());
            }

            if (giteSearched.getDepartement() == null)
                stm.setNull(7, Types.VARCHAR);
            else  {stm.setString (7,giteSearched.getDepartement().getCodeDepartement());}

            if (giteSearched.getRegion() == null)
                stm.setNull(8, Types.VARCHAR);
            else  {stm.setString (8,giteSearched.getRegion().getCodeRegion());}

            stm.setNull(9, Types.INTEGER);
            stm.setBigDecimal(10,giteSearched.getPrixMax() );
            stm.setNull(11, Types.DECIMAL);
            if (giteSearched.getPrestationsSearched().equals(""))
                stm.setNull(12,Types.VARCHAR);
            else { stm.setString(12,giteSearched.getPrestationsSearched());}
            if (giteSearched.getRayon()== null)
                stm.setNull(13,Types.VARCHAR);
            else {
                stm.setString(13,giteSearched.getRayon());
            }


            System.out.println(stm);
            stm.execute();
            rs= stm.getResultSet();
            Gite gite;
            Ville ville;
            Departement departement;
            while(rs.next()){

                gite = new Gite(rs.getInt(1), rs.getString(2));

                gite.setNombreChambres(rs.getInt(4));
                gite.setNombreCouchages(rs.getInt(5));
                gite.setAdresse1(rs.getString(7));
                gite.setAdresse2(rs.getString(8));
                gite.setAdresse3(rs.getString(9));
                gite.setLibelleVoie(rs.getString(10));
                gite.setCodePostale(rs.getString(11));
                gite.setSurfaceHabitable(rs.getFloat(3));
                gite.setSiteWeb(rs.getString(6));
                ville= new Ville(rs.getString(12), rs.getString(13),
                        rs.getString(14),rs.getFloat(15),
                        rs.getFloat(16));
                gite.setVille(ville);
                liste.add(gite);

            }
            rs.close();
            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return liste;
    }



    @Override
    public ArrayList<Gite> getAll() {
        ResultSet rs;
        ArrayList<Gite> liste = new ArrayList<Gite>();
        try
        {
            Statement stmt = null;

            stmt = connexion.createStatement();

            String strCmd = "SELECT g.id_gite,nom_gite,\n" +
                    "                        surface_habitable,nombre_chambres, nombre_couchage,site_web,adresse_1,adresse_2,\n" +
                    "                                                   adresse_3,libelle_voie,g.code_postale,\n" +
                    "                        c2.code_insee,c2.nom_commune,c2.code_postale,c2.latitude,c2.longitude,\n" +
                    "                        d.code_departement,d.nom_departement\n" +
                    "                        from gite\n" +
                    "                        as G join commune c2 on G.code_departement = c2.code_departement and G.code_insee = c2.code_insee\n" +
                    "                        join  departement d on c2.code_departement = d.code_departement\n" +
                    "                        order by id_gite";

            rs = stmt.executeQuery(strCmd);
            Gite  gite ;
            Ville ville;
            while (rs.next())
            { gite = new Gite(rs.getInt(1), rs.getString(2));
                gite.setNombreChambres(rs.getInt(4));
                gite.setNombreCouchages(rs.getInt(5));
                gite.setAdresse1(rs.getString(7));
                gite.setAdresse2(rs.getString(8));
                gite.setAdresse3(rs.getString(9));
                gite.setLibelleVoie(rs.getString(10));
                gite.setCodePostale(rs.getString(11));
                gite.setSurfaceHabitable(rs.getFloat(3));
                gite.setSiteWeb(rs.getString(6));
                ville= new Ville(rs.getString(12), rs.getString(13),
                        rs.getString(14),rs.getFloat(15),
                        rs.getFloat(16));
                gite.setVille(ville);

                liste.add(gite);
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println(liste.size());

        return liste;
    }

    private Tarification recupererTarifGite(int idGite) {
        ResultSet rs;
        Tarification  tarification = new Tarification(null);
        try {
            Statement stmt = null;

            stmt = connexion.createStatement();

            String strCmd = " select t.tarif_hebdomadaire,p.id_periode, p.numero_semaine, tp.id_type_periode,tp.libelle_periode from   gite as G\n" +
                    "                                       JOIN tarifer as t on t.id_gite = g.id_gite\n" +
                    "                                       join periode p on p.id_periode= t.id_periode\n" +
                    "                                       join type_periode tp on p.id_type_periode = tp.id_type_periode\n" +
                    "where    (CAST (to_char(now(),'WW') AS INTEGER) = CAST (p.numero_semaine AS INTEGER)) and  G.id_gite = "+idGite;
            rs = stmt.executeQuery(strCmd);


            Periode periode;
            while (rs.next()){
                tarification.setTarifHebdomadaire (rs.getBigDecimal(1));
                periode = new Periode(rs.getInt(2), rs.getString(3));
                tarification.setPeriode(periode);
                periode.setTypePeriode(new TypePeriode(rs.getInt(4), rs.getString(5)));

            }
            rs.close();
            stmt.close();
        } catch (Exception error) {

            System.out.println("Impossible de récuperer la liste des Service_Equipement" + error);
        }
        return tarification;
    }

    public ArrayList getValeurMaxMin() {
        ResultSet rs;
        ArrayList liste = new ArrayList();
        try
        {
            Statement stmt = null;

            stmt = connexion.createStatement();

            String strCmd = " select * from public.values_Max_Min()";

            rs = stmt.executeQuery(strCmd);
            while (rs.next()) {
                liste.add(rs.getInt(1));
                liste.add(rs.getInt(2));
                liste.add(rs.getInt(3));
                liste.add(rs.getInt(4));
                liste.add(rs.getInt(5));
                liste.add(rs.getInt(6));
                liste.add(rs.getInt(7));
                liste.add(rs.getInt(8));
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return liste;

    }
    public int getChambreMax() {
        return chambreMax;
    }

    public void setChambreMax(int chambreMax) {
        this.chambreMax = chambreMax;
    }

    public int getChambremin() {
        return chambremin;
    }

    public void setChambremin(int chambremin) {
        this.chambremin = chambremin;
    }

    private ArrayList<Personne> listerProprietaire(int idGite ) {
        ResultSet rs;
        ArrayList<Personne> liste = new ArrayList<>();
        try {
            Statement stmt = null;

            stmt = connexion.createStatement();

            String strCmd = "select  p.id_personne,p.nom_personne,p.prenom_personne,p.email from gite as G\n" +
                    "                    JOIN avoir A ON A.id_gite=g.id_gite \n " +
                    "                    join personne p on p.id_personne=a.id_personne \n" +

                    "                   where g.id_gite = "+ idGite;

            rs = stmt.executeQuery(strCmd);
            Personne  personne ;
            EquipementService equipementService;
            while (rs.next()){
                personne = new Personne(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4));
                personne.setTelephone(listerTelephone(rs.getInt(1)));
                personne.setDisponibilite(listerDisponibilite(rs.getInt(1)));
                liste.add(personne);
            }
            rs.close();
            stmt.close();
        } catch (Exception error) {
            System.out.println("Impossible de récuperer la liste des Proprietaires " + error);
        }
        return liste;
    }
    private ArrayList<Personne> listerGerants(int idGite) {
        ResultSet rs;
        ArrayList<Personne> liste = new ArrayList<>();


        try {
            Statement stmt = null;

            stmt = connexion.createStatement();

            String strCmd = "select  p.id_personne,p.nom_personne,p.prenom_personne,p.email from gite as G\n" +
                    "                    JOIN avoir A ON A.id_gite=g.id_gite \n " +
                    "                    join personne p on p.id_personne=a.id_personne \n" +

                    "                   where g.id_gite = "+ idGite;

            rs = stmt.executeQuery(strCmd);
            Personne  personne ;
            EquipementService equipementService;
            while (rs.next()){
                personne = new Personne(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4));
                personne.setTelephone(listerTelephone(rs.getInt(1)));
                personne.setDisponibilite(listerDisponibilite(rs.getInt(1)));
                liste.add(personne);
            }
            rs.close();
            stmt.close();
        } catch (Exception error) {
            System.out.println("Impossible de récuperer la liste des  gerants " + error);
        }
        return liste;
    }

    private ArrayList<Disponibilite> listerDisponibilite(int idPersonne) {
        ResultSet rs;
        ArrayList<Disponibilite> liste = new ArrayList<>();


        try {
            Statement stmt = null;

            stmt = connexion.createStatement();

            String strCmd = "select  d.id_disponibilite,to_char(TO_DATE(d.jour,'DD'), 'TMDay ') ,\n" +
                    "       to_char(D.heure_debut,'HH24:MM'), to_char(d.heure_fin,'HH24:MM') from personne as p\n" +
                    "                   JOIN  disponibilite d on p.id_personne = d.id_personne\n" +
                    "                where p.id_personne =" +idPersonne + " order by d.jour";

            rs = stmt.executeQuery(strCmd);
            Disponibilite disponibilite ;

            while (rs.next()){
                disponibilite = new Disponibilite (rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4));
                System.out.println(disponibilite);

                liste.add(disponibilite);
            }

            rs.close();
            stmt.close();
        } catch (Exception error) {
            System.out.println("Impossible de récuperer la liste des  Disponibilité " + error);
        }
        return liste;

    }

    private ArrayList<Telephone> listerTelephone(int idPersonne) {
        ResultSet rs;
        ArrayList<Telephone> liste = new ArrayList<>();


        try {
            Statement stmt = null;

            stmt = connexion.createStatement();

            String strCmd = "select  tel.id_telephone,tel.type_telephone,tel.numero from personne as p\n" +
                    "                    JOIN possede ps ON ps.id_personne=p.id_personne \n " +
                    "                    join telephone TEL on TEL.id_telephone=PS.id_telephone \n" +
                    "                   where p.id_personne = "+ idPersonne;

            rs = stmt.executeQuery(strCmd);
            Telephone telephone ;
            EquipementService equipementService;
            while (rs.next()){
                telephone = new Telephone (rs.getInt(1),rs.getString(2),rs.getString(3) );
                liste.add(telephone);
            }
            rs.close();
            stmt.close();
        } catch (Exception error) {
            System.out.println("Impossible de récuperer la liste des    Telephones " + error);
        }
        return liste;
    }


    private ArrayList<Prestation> listerPrestations(int idGite) {
        ResultSet rs;
        ArrayList<Prestation> liste = new ArrayList<>();


        try {
            Statement stmt = null;

            stmt = connexion.createStatement();

            String strCmd = "select p.prix,ees.id_service, ees.nom_service,ees.id_type_service, tes.libelle  from gite as G\n" +
                    "                    JOIN proposer P  on G.id_gite = P.id_gite\n" +
                    "                    join equipement_et_service ees on P.id_service = ees.id_service\n" +
                    "join type_equipement_service tes on ees.id_type_service = tes.id_type_service" +
                    " where g.id_gite = "+ idGite  ;
            rs = stmt.executeQuery(strCmd);
            Prestation  prestation ;
            EquipementService equipementService;
            while (rs.next()){
                prestation = new Prestation(rs.getBigDecimal(1));
                equipementService= new EquipementService(rs.getInt(2),rs.getString(3));
                prestation.setEquipementService(equipementService);
                equipementService.setTypeEquipementService(new TypeEquipement(rs.getInt(4),rs.getString(5)));
                liste.add(prestation);

            }
            rs.close();
            stmt.close();
        } catch (Exception error) {
            System.out.println("Impossible de récuperer la liste des Service_Equipement" + error);
        }
        return liste;
    }

    @Override
    public boolean insert(Gite gite) {
        try {
            String strCmd = "  insert into gite  (nom_gite ,surface_habitable,nombre_chambres,nombre_couchage,site_web," +
                    "adresse_1,adresse_2,libelle_voie,adresse_3,code_postale,code_departement,code_insee" +
                    " ) values (?,?,?,?,?,?,?,?,?,?,?,?) ";
            PreparedStatement ps = connexion.prepareCall(strCmd);
            ps.setString(1, gite.getNomGite());
            ps.setFloat(2, gite.getSurfaceHabitable());
            ps.setInt(3, gite.getNombreChambres());
            ps.setInt(4, gite.getNombreCouchages());
            ps.setString(5, gite.getSiteWeb());
            ps.setString(6, gite.getAdresse1());
            ps.setString(7, gite.getAdresse2());
            ps.setString(8, gite.getLibelleVoie());
            ps.setString(9, gite.getAdresse3());
            ps.setString(10, gite.getCodePostale());
            ps.setString(12, gite.getVille().getCodeInsee());
            System.out.println(gite.getVille().getDepartement().getCodeDepartement());
            ps.setString(11, gite.getVille().getDepartement().getCodeDepartement());

            ps.execute();
            ps.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }}

    @Override
    public boolean update(Gite gite) {
        try {String strCmd = "  UPDATE gite set  " +
                "nom_gite= ?," +
                "surface_habitable= ?," +
                "nombre_chambres= ?," +
                "nombre_couchage= ?," +
                "site_web= ?," +
                "adresse_1= ?," +
                "adresse_2= ?," +
                "libelle_voie= ?," +
                "adresse_3= ?," +
                "code_postale= ?," +
                "code_departement= ?," +
                "code_insee where id_gite ="+ gite.getIdGite();
            PreparedStatement ps = connexion.prepareCall(strCmd) ;
            ps.setString(1, gite.getNomGite());
            ps.setFloat(2, gite.getSurfaceHabitable());
            ps.setInt(3, gite.getNombreChambres());
            ps.setInt(4, gite.getNombreCouchages());
            ps.setString(5, gite.getSiteWeb());
            ps.setString(6, gite.getAdresse1());
            ps.setString(7, gite.getAdresse2());
            ps.setString(8, gite.getLibelleVoie());
            ps.setString(9, gite.getAdresse3());
            ps.setString(10, gite.getCodePostale());
            ps.setString(11, gite.getVille().getDepartement().getCodeDepartement());
            ps.setString(12, gite.getVille().getCodeInsee());
            ps.execute();
            ps.close();
            return true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean delete(Gite gite) {
        String strCmd = "  delete  from gite  where id_gite=? ";
        PreparedStatement ps;
        try {
            ps = this.connexion.prepareCall(strCmd);
            ps.setInt(1, gite.getIdGite());
            ps.execute();
            ps.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

    }


}
