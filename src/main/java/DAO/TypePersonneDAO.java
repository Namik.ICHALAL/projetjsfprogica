package DAO;

import metier.TypePersonne;

import java.sql.Connection;
import java.util.ArrayList;

public class TypePersonneDAO extends DAO<TypePersonne> {
    public TypePersonneDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public TypePersonne getByID(int id) {
        return null;
    }

    @Override
    public ArrayList<TypePersonne> getAll() {
        return null;
    }

    @Override
    public boolean insert(TypePersonne objet) {
        return false;
    }

    @Override
    public boolean update(TypePersonne objet) {
        return false;
    }

    @Override
    public boolean delete(TypePersonne objet) {
        return false;
    }
}
