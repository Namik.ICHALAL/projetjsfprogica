package DAO;

import metier.Departement;
import metier.Ville;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class VilleDAO extends DAO<Ville> {

    public VilleDAO(Connection connexion) {
        super(connexion);
    }

    @Override
    public Ville getByID(int id) {
        return null;
    }

    @Override
    public ArrayList<Ville> getAll() {
        ResultSet rs;
        ArrayList<Ville> liste = new ArrayList<Ville>();
        try
        {
            Statement stmt = null;

            stmt = connexion.createStatement();

            String strCmd = "SELECT  c.code_insee,c.nom_commune,C.code_postale,c.latitude,c.longitude,\n" +
                    "                    d.code_departement,d.nom_departement from commune  c\n" +
                    "                     join departement d on c.code_departement = d.code_departement\n" +
                    "                    order by nom_commune";

            rs = stmt.executeQuery(strCmd);
            Ville ville   ;
            while (rs.next())
            { ville = new Ville(rs.getString(1), rs.getString(2),
                    rs.getString(3),rs.getFloat(4),
                    rs.getFloat(5));
                ville.setDepartement(new Departement(rs.getString(6),rs.getString(7)));
                liste.add(ville);

            }
            rs.close();
            stmt.close();
        } catch (Exception error) {
            System.out.println("récuperation des données ville impossible  "+ error);
        }
        return liste;
    }
    public ArrayList getByPostalCode(String codePostale){
        ResultSet rs;
        ArrayList<Ville> liste = new ArrayList<Ville>();
        try
        {
            Statement stmt = null;

            stmt = connexion.createStatement();

            String strCmd = "SELECT  c.code_insee,c.nom_commune,c.code_postale,c.latitude,c.longitude, d.code_departement,d.nom_departement  from commune as c\n" +
                    "    join departement as d ON d.code_departement = c.code_departement\n" +
                    "where code_postale like'"+codePostale+"' order by nom_commune";

            rs = stmt.executeQuery(strCmd);
            Ville ville   ;
            while (rs.next())
            { ville = new Ville(rs.getString(1), rs.getString(2),
                    rs.getString(3),rs.getFloat(4),
                    rs.getFloat(5));
                ville.setDepartement(new Departement(rs.getString(6), rs.getString(7)));
                System.out.println(ville);
                liste.add(ville);

            }
            rs.close();
            stmt.close();
        } catch (Exception error) {
            System.out.println("récuperation des données ville impossible du code postale impossible  "+ error);
        }
        return liste;
    }

    public ArrayList getByStringValue(String string){
        ResultSet rs;
        ArrayList<Ville> liste = new ArrayList<Ville>();
        try
        {
            Statement stmt = null;

            stmt = connexion.createStatement();

            String strCmd = "SELECT  c.code_insee,c.nom_commune,c.code_postale,c.latitude,c.longitude, d.code_departement,d.nom_departement  from commune as c \n" +
                    "                        join departement as d ON d.code_departement = c.code_departement\n" +
                    "                    where nom_commune like   ' "+ string+"%'  order by nom_commune;";

            rs = stmt.executeQuery(strCmd);
            Ville ville   ;
            while (rs.next())
            { ville = new Ville(rs.getString(1), rs.getString(2),
                    rs.getString(3),rs.getFloat(4),
                    rs.getFloat(5));
                ville.setDepartement(new Departement(rs.getString(6), rs.getString(7)));
                System.out.println(ville);
                liste.add(ville);

            }
            rs.close();
            stmt.close();
        } catch (Exception error) {
            System.out.println("récuperation des données ville impossible du code postale impossible  "+ error);
        }
        return liste;
    }




    @Override
    public boolean insert(Ville objet) {
        return false;
    }

    @Override
    public boolean update(Ville objet) {
        return false;
    }

    @Override
    public boolean delete(Ville objet) {
        return false;
    }
}
