package DAO;



import java.sql.Connection;

public class DAOFactory {


    public static Connection connexion;

    static {
        try {
            connexion = DataBaseConnect.getInstance();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static GiteDAO getGiteDAO() {
        return new GiteDAO(connexion);
    }

    public static VilleDAO getVilleDAO() {
        return new VilleDAO(connexion);
    }
    public static DepartementDAO getDepartementDAO () {
        return new DepartementDAO(connexion);
    }
    public static RegionDAO getRegionDAO() {
        return new RegionDAO(connexion);
    }
    public static PersonneDAO getPersonneDAO() {
        return new PersonneDAO(connexion);
    }
    public static TeleponeDAO getTeleponeDAO() {
        return new TeleponeDAO(connexion);
    }
    public static TypePersonneDAO getTypePersonneDAO() {
        return new TypePersonneDAO(connexion);
    }
        public static DisponibiliteDAO getDisponibiliteDAO(){
            return new DisponibiliteDAO(connexion);
        }
    public static PeriodeDAO getPeriodeDAO(){
        return new PeriodeDAO(connexion);
    }

    public static TypePeriodeDAO getTypePeriodeDAO(){
        return new  TypePeriodeDAO(connexion);
    }
    public static EquipementServiceDAO getEquipementServiceDAO(){
        return new EquipementServiceDAO(connexion);

    }
    public static TypeEquipementServiceDAO getTypeEquipementServiceDAO(){
return   new TypeEquipementServiceDAO(connexion);
    }
}
