package metier;


public class Disponibilite {

   private int idDisponibilite;
   private String jour;
   private String heureDebut;
   private String heureFin;

    public Disponibilite(int idDisponibilite, String jour, String heureDebut, String heureFin) {
        this.idDisponibilite = idDisponibilite;
        this.jour = jour;
        this.heureDebut = heureDebut;
        this.heureFin = heureFin;
    }

    public int getIdDisponibilite() {
        return idDisponibilite;
    }

    public Disponibilite setIdDisponibilite(int idDisponibilite) {
        this.idDisponibilite = idDisponibilite;
        return this;
    }

    public String getJour() {
        return jour;
    }

    public Disponibilite setJour(String jour) {
        this.jour = jour;
        return this;
    }

    public String getHeureDebut() {
        return heureDebut;
    }

    public Disponibilite setHeureDebut(String heureDebut) {
        this.heureDebut = heureDebut;
        return this;
    }

    public String getHeureFin() {
        return heureFin;
    }

    public Disponibilite setHeureFin(String heureFin) {
        this.heureFin = heureFin;
        return this;
    }
}
