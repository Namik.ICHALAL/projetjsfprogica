package metier;


import java.util.ArrayList;

public class Personne {
    private int idPersonne;
    private String nomPersonne;
    private String prenomPersonne;
    private String email;
    private ArrayList<Telephone> telephone;
    private ArrayList<Disponibilite> disponibilite;
    private TypePersonne typePersonne;

	public Personne(int idPersonne, String nomPersonne, String prenomPersonne, String email) {
		this.idPersonne = idPersonne;
		this.nomPersonne = nomPersonne;
		this.prenomPersonne = prenomPersonne;
		this.email = email;
		this.telephone = new ArrayList<Telephone>();
		this.disponibilite = new ArrayList<Disponibilite>();
		this.typePersonne = typePersonne;
	}

	public int getIdPersonne() {
		return idPersonne;
	}

	public Personne setIdPersonne(int idPersonne) {
		this.idPersonne = idPersonne;
		return this;
	}

	public String getNomPersonne() {
		return nomPersonne;
	}

	public Personne setNomPersonne(String nomPersonne) {
		this.nomPersonne = nomPersonne;
		return this;
	}

	public String getPrenomPersonne() {
		return prenomPersonne;
	}

	public Personne setPrenomPersonne(String prenomPersonne) {
		this.prenomPersonne = prenomPersonne;
		return this;
	}

	public String getEmail() {
		return email;
	}

	public Personne setEmail(String email) {
		this.email = email;
		return this;
	}

	public ArrayList<Telephone> getTelephone() {
		return telephone;
	}

	public Personne setTelephone(ArrayList<Telephone> telephone) {
		this.telephone = telephone;
		return this;
	}

	public ArrayList<Disponibilite> getDisponibilite() {
		return disponibilite;
	}

	public Personne setDisponibilite(ArrayList<Disponibilite> disponibilite) {
		this.disponibilite = disponibilite;
		return this;
	}

	public TypePersonne getTypePersonne() {
		return typePersonne;
	}

	public Personne setTypePersonne(TypePersonne typePersonne) {
		this.typePersonne = typePersonne;
		return this;
	}
}
