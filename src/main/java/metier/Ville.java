package metier;



import java.util.ArrayList;

public class Ville {
    private String codeInsee;
    private String nomVille;
    private String codePostale;
    private float latitude;
    private float longitude;
    private Departement departement;

    public Ville(String codeInsee, String nomVille, String codePostale, float latitude, float longitude) {
        this.codeInsee = codeInsee;
        this.nomVille = nomVille;
        this.codePostale = codePostale;
        this.latitude = latitude;
        this.longitude = longitude;
        this.departement =departement;
    }

    public String getCodeInsee() {
        return codeInsee;
    }

    public Ville setCodeInsee(String codeInsee) {
        this.codeInsee = codeInsee;
        return this;
    }

    public String getNomVille() {
        return nomVille;
    }

    public Ville setNomVille(String nomVille) {
        this.nomVille = nomVille;
        return this;
    }

    public String getCodePostale() {
        return codePostale;
    }

    public Ville setCodePostale(String codePostale) {
        this.codePostale = codePostale;
        return this;
    }

    public float getLatitude() {
        return latitude;
    }

    public Ville setLatitude(float latitude) {
        this.latitude = latitude;
        return this;
    }

    public float getLongitude() {
        return longitude;
    }

    public Ville setLongitude(float longitude) {
        this.longitude = longitude;
        return this;
    }


    public Departement getDepartement() {
        return departement;
    }

    public void setDepartement(Departement departement) {
        this.departement = departement;
    }

    @Override
    public String toString() {
        return  nomVille ;

    }
}
