package metier;



import java.util.ArrayList;

public class EquipementService {
    private int idServiceEquipement;
    private String nomServiceEquipement;
    private TypeEquipement typeEquipementService;

	public EquipementService(int idServiceEquipement, String nomServiceEquipement) {
		this.idServiceEquipement = idServiceEquipement;
		this.nomServiceEquipement = nomServiceEquipement;
		this.typeEquipementService = typeEquipementService;
	}

	public int getIdServiceEquipement() {
		return idServiceEquipement;
	}

	public EquipementService setIdServiceEquipement(int idServiceEquipement) {
		this.idServiceEquipement = idServiceEquipement;
		return this;
	}

	public String getNomServiceEquipement() {
		return nomServiceEquipement;
	}

	public EquipementService setNomServiceEquipement(String nomServiceEquipement) {
		this.nomServiceEquipement = nomServiceEquipement;
		return this;
	}

	public TypeEquipement getTypeEquipementService() {
		return typeEquipementService;
	}

	public EquipementService setTypeEquipementService(TypeEquipement typeEquipementService) {
		this.typeEquipementService = typeEquipementService;
		return this;
	}

	@Override
	public String toString() {
		return "EquipementService{" +
				"idServiceEquipement=" + idServiceEquipement +
				", nomServiceEquipement='" + nomServiceEquipement + '\'' +
				", typeEquipementService=" + typeEquipementService +
				'}';
	}
}
