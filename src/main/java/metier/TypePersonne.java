package metier;

public class TypePersonne {
    private int idTypePersonne;
    private String nomTypePersonne;

	public TypePersonne(int idTypePersonne, String nomTypePersonne) {
		this.idTypePersonne = idTypePersonne;
		this.nomTypePersonne = nomTypePersonne;
	}

	public int getIdTypePersonne() {
		return idTypePersonne;
	}

	public TypePersonne setIdTypePersonne(int idTypePersonne) {
		this.idTypePersonne = idTypePersonne;
		return this;
	}

	public String getNomTypePersonne() {
		return nomTypePersonne;
	}

	public TypePersonne setNomTypePersonne(String nomTypePersonne) {
		this.nomTypePersonne = nomTypePersonne;
		return this;
	}

	@Override
	public String toString() {
		return nomTypePersonne ;
	}
}
