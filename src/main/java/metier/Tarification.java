package metier;

import java.math.BigDecimal;

public class Tarification {
    private BigDecimal tarifHebdomadaire;
    private Periode periode;

    public Tarification(BigDecimal tarif) {
        this.tarifHebdomadaire = tarif;
        this.periode = periode;
    }

    public BigDecimal getTarifHebdomadaire() {
        return tarifHebdomadaire;
    }

    public Tarification setTarifHebdomadaire(BigDecimal tarif) {
        this.tarifHebdomadaire = tarif;
        return this;
    }

    public Periode getPeriode() {
        return periode;
    }

    public Tarification setPeriode(Periode periode) {
        this.periode = periode;
        return this;
    }
}

