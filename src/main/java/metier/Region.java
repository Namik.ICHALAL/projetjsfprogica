package metier;

import java.util.ArrayList;

public class Region {
    private String codeRegion;
    private String nomRegion;
    private ArrayList<Departement> departementlistOfR;

    public Region(String codeRegion, String nomRegion) {
        this.codeRegion = codeRegion;
        this.nomRegion = nomRegion;
        this.departementlistOfR = new ArrayList<>();

    }

    public String getCodeRegion() {
        return codeRegion;
    }

    public ArrayList<Departement> getDepartementlistOfR() {
        return departementlistOfR;
    }

    public void setDepartementlistOfR(ArrayList<Departement> departementlistOfR) {
        this.departementlistOfR = departementlistOfR;
    }

    public Region setCodeRegion(String codeRegion) {
        this.codeRegion = codeRegion;
        return this;
    }

    public String getNomRegion() {
        return nomRegion;
    }

    public Region setNomRegion(String nomRegion) {
        this.nomRegion = nomRegion;
        return this;
    }

    @Override
    public String toString() {
        return  nomRegion ;
    }

}

