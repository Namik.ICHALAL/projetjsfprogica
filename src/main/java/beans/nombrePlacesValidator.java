package beans;

import DAO.DAOFactory;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ManagedBean
@RequestScoped
public class nombrePlacesValidator {
private GitesBeans gitesBeans = new GitesBeans();



        @Min(value=0, message="Veuillez saisir une valeur supérieur a 0")
        @Max(value= 3, message="Désolé on a p")
        private int NombreDePlace;

    public int getNombreDePlace() {
        return NombreDePlace;
    }

    public nombrePlacesValidator setNombreDePlace(int nombreDePlace) {
        NombreDePlace = nombreDePlace;
        return this;
    }
}
