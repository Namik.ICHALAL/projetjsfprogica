package beans;


import DAO.DAOFactory;
import metier.Departement;
import metier.Gite;
import metier.Region;
import metier.Ville;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.math.BigDecimal;
import java.util.ArrayList;


@ManagedBean(name = "gitesBeans")
@RequestScoped
public class GitesBeans
{
    private ArrayList<Gite> gites;
    private ArrayList<Ville> villes;
    private ArrayList<Departement> departements;
    private ArrayList<Region> regions;
    private  Ville villeSelected;
    private Departement departementSelected;
    private ArrayList<Ville> villesByName;
    private String villeTextValue;
    private  int nombrePlaceMin;
    private GiteSearched giteSearched;
    private ArrayList <Integer> maxMinlist;

    public GitesBeans() {
        gites = DAOFactory.getGiteDAO().getAll();
        villes= DAOFactory.getVilleDAO().getAll();
        departements= DAOFactory.getDepartementDAO().getAll();
        regions= DAOFactory.getRegionDAO().getAll();
        villeSelected = new Ville(null, "","",0,0 );
        departementSelected = new Departement(null,"");
       villesByName = new ArrayList<Ville>();
       giteSearched = new GiteSearched();
       maxMinlist = DAOFactory.getGiteDAO().getValeurMaxMin();
    }

    public ArrayList<Gite> getGites() {
        System.out.println("gites dl");
        return gites;

    }

    public void getVillesById() {
        if (!villeTextValue.equals(null))
        villesByName = DAOFactory.getVilleDAO().getByStringValue(villeTextValue);
    }

    public String getVilleTextValue() {
        return villeTextValue;
    }

    public GitesBeans setVilleTextValue(String villeTextValue) {
        this.villeTextValue = villeTextValue;
        return this;
    }

    public ArrayList<Ville> getVillesByName() {
        return villesByName;
    }

    public GitesBeans setVillesByName(ArrayList<Ville> villesByName) {
        this.villesByName = villesByName;
        return this;
    }

    public Departement getDepartementSelected() {
        return departementSelected;
    }

    public GitesBeans setDepartementSelected(Departement departementSelected) {
        this.departementSelected = departementSelected;
        return this;
    }

    public GiteSearched getGiteSearched() {
        System.out.println(".........get..........");
        return giteSearched;
    }

    public GitesBeans setGiteSearched(GiteSearched giteSearched) {
        this.giteSearched = giteSearched;
        System.out.println(".........set..........");
        return this;
    }

    public int getNombrePlaceMin() {
        return nombrePlaceMin;
    }

    public GitesBeans setNombrePlaceMin(int nombrePlaceMin) {
        nombrePlaceMin = nombrePlaceMin;
        return this;
    }
    public void getListGitesSearched(){

         if (nombrePlaceMin!=0){
         giteSearched.setNombreChambresMax(nombrePlaceMin+2);}
         else {giteSearched.setNombreChambresMax((Integer) maxMinlist.get(1));}

         giteSearched.setNombreChambresMin((Integer) maxMinlist.get(0));
         giteSearched.setNombreCouchagesMax((Integer) maxMinlist.get(3));
         giteSearched.setNombreCouchagesMin((Integer) maxMinlist.get(2));
         giteSearched.setPrixMax(BigDecimal.valueOf(maxMinlist.get(5)));
         giteSearched.setPrixMin(BigDecimal.valueOf(maxMinlist.get(4)));
        System.out.println(giteSearched);
         gites = DAOFactory.getGiteDAO().getLike(giteSearched);

    }

    public ArrayList<Ville> getVilles() {
        return villes;
    }

    public ArrayList<Departement> getDepartements() {
        return departements;
    }

    public ArrayList<Region> getRegions() {
        return regions;
    }

    public Ville getVilleSelected() {
        return villeSelected;
    }

    public GitesBeans setVilleSelected(Ville villeSelected) {
        this.villeSelected = villeSelected;
        return this;
    }
}
